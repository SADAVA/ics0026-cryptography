
// S.

// This file contains only nonrelated to cryptography code
// [UNIVERSITY] TalTech 2020/2021 Spring semester
// [AUTHOR] Aleksander Tkatšenko, Uni-ID: 192901IVSB
// [NOTE] Node version: v12.20.1

"use strict"

module.exports.ask = require("./ask")

/**
 * @template T
 * @param {{[key: T]: number}} obj 
 * @returns {T}
 */
module.exports.findMode = obj => {
	let mode = null
	let count = 0

	Object.keys(obj).forEach(prop => {
		if (obj[prop] > count) {
			mode = prop
			count = obj[prop]
		}
	})

	const parsed = parseInt(mode)

	return isNaN(parsed) !== true ? parsed : mode
}

/**
 * Encode into hex
 * 
 * @param {number} num 0 to 255 positive number
 * @returns {string} 2 char long string
 */
module.exports.hexEncode = nums => nums.map(num => num.toString(16).padStart(2, '0')).join('')

/**
 * Decodes from hex
 * 
 * @param {string} string String of hex
 * @returns {number[]} Returns an array of 0 - 255 positive numbers
 */
module.exports.hexDecode = string =>
	module.exports.splitInParts(string.split(''), 2)
		.map(arr => arr.join(''))
		.map(str => parseInt(str, 16))

/**
 * Decode from string
 * 
 * @param {string} string 
 * @returns {number[]}
 */
module.exports.stringDecode = string => string.split('').map(module.exports.charDecode)

/**
 * Decode from char
 *  
 * @param {string} char Single char string
 * @returns {number}
 */
module.exports.charDecode = char => char.charCodeAt(0)

/**
 * Encode into string
 * 
 * @param {number[]} codes 
 * @returns {string}
 */
module.exports.stringEncode = codes => codes.map(module.exports.charEncode).join('')

/**
 * Encode into char
 * 
 * @param {number} code 
 * @returns {string} single char string
 */
module.exports.charEncode = code => String.fromCharCode(code)

/**
 * @template F
 * @template S
 * @template R
 * @param {F[]} f 
 * @param {S[]} s 
 * @param {(a: F, b: S, index: number) => R} cb 
 * @returns {R[]}
 */
module.exports.zipTile = (f, s, cb) => {
	// If s is longer than f, swap places so that we map over the longest one
	if (f.length < s.length) [f, s] = [s, f]

	let index = 0

	return f.map((val, i) => cb(val, s[i % s.length], index++))
}

module.exports.cross = (f, s, cb) => {
	if (typeof s === "function") {
		cb = s
		s = f
	}

	return f.map(a => s.map(b => cb(a, b)))
}

/**
 * @template F
 * @template S
 * @template R
 * @param {F[]} f 
 * @param {S[]} s 
 * @param {(a: F, b: S, index: number) => R} cb 
 * @returns {R[]}
 */
module.exports.zip = (f, s, cb) => {
	// Ensure f is smaller than s
	if (f.length > s.length) [f, s] = [s, f]

	let index = 0

	return f.map((val, i) => cb(val, s[i], index++))
}

module.exports.biggestIndex = obj =>
	Object.keys(obj)
		.map(key => parseInt(key))
		.filter(key => isNaN(key) !== true)
		.reverse()
	[0]

/**
 * Splits given array into given size parts
 * 
 * @param {any[]} array Array of any items
 * @param {number} size Size of desired parts
 * @returns {Any[][]} Array of arrays
 */
module.exports.splitInParts = (array, size) =>
	// Example inputs: array: [1, 2, 3, 4], size: 2
	// Create new array with null, of desired size
	// Example: [null, null]
	Array(Math.floor(array.length / size)).fill(null)
		// Iter per each slot in new array
		// Example after all iterations: [[1, 2], [3, 4]]
		.map(_ =>
			// Get and remove size times item from input array
			// Example on first iteration: [1, 2]
			// Example on second iteration: [2, 3]
			array.splice(0, size))

// EOF