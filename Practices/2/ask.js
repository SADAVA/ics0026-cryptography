
// S.

"use strict";

const readline = require("readline");

/**
 * @param {string} question 
 * @returns {Promise<string|number>}
 */
function ask(question) {
	return new Promise((resolve, reject) => {
		const int = readline.createInterface({
			input: process.stdin,
			output: process.stdout
		});

		int.question(question, result => {
			let number = parseNumber(result);

			if (number !== undefined) resolve(number);
			else resolve(result);

			int.close();
		});
	});
}

const truelishValues = ["yes", "ye", "y"];
const falselishValues = ["no", "n"];

/**
 * @param {string} question 
 * @returns {Promise<boolean>} whether user answered yes
 */
async function yes(question) {
	let res = await ask(question + "[y/N]");

	return truelishValues.indexOf(res.toLowerCase()) !== -1;
}

/**
 * @param {string} question 
 * @returns {Promise<boolean>} whether user answered no
 */
async function no(question) {
	let res = await ask(question + "[Y/n]");

	return falselishValues.indexOf(res.toLowerCase()) !== -1;
}

function parseNumber(num) {
	let i = parseInt(num);

	if (isNaN(i) === true) {
		return undefined;
	} else return i;
}

module.exports = ask;
module.exports.yes = yes;
module.exports.no = no;

// EOF