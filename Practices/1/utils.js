
// S.

// [UNIVERSITY] TalTech 2020/2021 Spring semester
// [AUTHOR] Aleksander Tkatšenko, Uni-ID: 192901IVSB
// [NOTE] Node version: v12.20.1

"use strict"

const BYTE_LENGTH = 8

/**
 * Transform string into an array of bits
 * 
 * @param {string} chars Text to transform into bits
 * @returns {(0|1)[]} Array of 0's and 1's
 */
module.exports.stringToBits = chars =>
	// Example input: chars: "Hello"
	// Saparate chars for each other
	// Example: [ 'H', 'e', 'l', 'l', 'o' ]
	chars.split('')
		// Turn chars into numbers
		// Example: [ 72, 101, 108, 108, 111 ]
		.map(char => char.charCodeAt(0))
		// Turn numbers into string representation of binary
		// Example: [ '1001000', '1100101', '1101100', '1101100', '1101111' ]
		.map(num => num.toString(2))
		// Ensure all strings are exactly one byte long
		// by adding as many 0's as nessesary
		// Example: [ '01001000', '01100101', '01101100', '01101100', '01101111' ]
		.map(string => '0'.repeat(BYTE_LENGTH - string.length) + string)
		// Split into array of chars representing bits
		// Example: [['0', '1', '0','0', '1', '0','0', '0'],['0', '1', '1','0', '0', '1','0', '1'],['0', '1', '1','0', '1', '1','0', '0'],['0', '1', '1','0', '1', '1','0', '0'],['0', '1', '1','0', '1', '1','1', '1']]sadava@whiteshmallow:~/Documents/Workspaces/Cryptography/Practices/1$ node utils.js[ 'H', 'e', 'l', 'l', 'o' ]sadava@whiteshmallow:~/Documents/Workspaces/Cryptography/Practices/1$ node utils.js[ 72, 101, 108, 108, 111 ]sadava@whiteshmallow:~/Documents/Workspaces/Cryptography/Practices/1$ node utils.js[ '1001000', '1100101', '1101100', '1101100', '1101111' ]sadava@whiteshmallow:~/Documents/Workspaces/Cryptography/Practices/1$ node utils.js[ '01001000', '01100101', '01101100', '01101100', '01101111' ]sadava@whiteshmallow:~/Documents/Workspaces/Cryptography/Practices/1$ node utils.js[['0', '1', '0','0', '1', '0','0', '0'],['0', '1', '1','0', '0', '1','0', '1'],['0', '1', '1','0', '1', '1','0', '0'],['0', '1', '1','0', '1', '1','0', '0'],['0', '1', '1','0', '1', '1','1', '1']]
		.map(string => string.split(''))
		// Turn all strings into ints
		// Example: [[0, 1, 0, 0,1, 0, 0, 0],[0, 1, 1, 0,0, 1, 0, 1],[0, 1, 1, 0,1, 1, 0, 0],[0, 1, 1, 0,1, 1, 0, 0],[0, 1, 1, 0,1, 1, 1, 1]]
		.map(arrs => arrs.map(str => parseInt(str)))
		// Join all inner arrays into one single
		// Example: [0, 1, 0, 0, 1, 0, 0, 0, 0, 1,1, 0, 0, 1, 0, 1, 0, 1, 1, 0,1, 1, 0, 0, 0, 1, 1, 0, 1, 1,0, 0, 0, 1, 1, 0, 1, 1, 1, 1]
		.flat()

/**
 * Transform array of bits into a string
 * 
 * @param {(0|1)[]} bits Array of 0's and 1's
 * @returns {string} Result string
 */
module.exports.bitsToString = bits =>
	// Example: bits: [0, 1, 0, 0, 1, 0, 0, 0, 0, 1,1, 0, 0, 1, 0, 1, 0, 1, 1, 0,1, 1, 0, 0, 0, 1, 1, 0, 1, 1,0, 0, 0, 1, 1, 0, 1, 1, 1, 1]
	// Split bits into chunks of BYTE_LENGTH
	// Example: [[0, 1, 0, 0,1, 0, 0, 0],[0, 1, 1, 0,0, 1, 0, 1],[0, 1, 1, 0,1, 1, 0, 0],[0, 1, 1, 0,1, 1, 0, 0],[0, 1, 1, 0,1, 1, 1, 1]]
	module.exports.splitInParts(bits, BYTE_LENGTH)
		// Join bytes into single strings
		// Example: [ '01001000', '01100101', '01101100', '01101100', '01101111' ]
		.map(bytes => bytes.join(''))
		// Parse binary strings into numbers
		// Example: [ 72, 101, 108, 108, 111 ]
		.map(bytes => parseInt(bytes, 2))
		// Transform numbers into chars
		// Example: [ 'H', 'e', 'l', 'l', 'o' ]
		.map(number => String.fromCharCode(number))
		// Join chars into one whole
		// Example: Hello
		.join('')

/**
 * Splits given array into given size parts
 * 
 * @param {any[]} array Array of any items
 * @param {number} size Size of desired parts
 * @returns {Any[][]} Array of arrays
 */
module.exports.splitInParts = (array, size) =>
	// Example inputs: array: [1, 2, 3, 4], size: 2
	// Create new array with null, of desired size
	// Example: [null, null]
	Array(Math.floor(array.length / size)).fill(null)
		// Iter per each slot in new array
		// Example after all iterations: [[1, 2], [3, 4]]
		.map(_ =>
			// Get and remove size times item from input array
			// Example on first iteration: [1, 2]
			// Example on second iteration: [2, 3]
			array.splice(0, size))

// EOF