
// S.

console.log(`
[UNIVERSITY] TalTech 2020/2021 Spring semester
[AUTHOR] Aleksander Tkatšenko, Uni-ID: 192901IVSB

[DESCRIPTION] Implementation of one time pad.
Just set the value of variables 'string' and 'key' and see the magic.
Alternatively you can use command line arguments to pass those variables:
> node one_time_pad.js "string variable" "key variable"

[NOTE] Tested Node version: v12.20.1 \n`)

"use strict"

const utils = require("./utils")

const string = process.argv[2] || "Hello world"
const key = process.argv[3] || "abc"

console.log(`[INFO] Selected string: ${string}, key: ${key}`)
console.log(`[INFO] Lengths: string: ${string.length} bytes, key: ${key.length} bytes`)
console.log(`[INFO] Binnary representation: string:`, utils.stringToBits(string), 'key:', utils.stringToBits(key))

const encrypted = encrypt(string, key)

console.log(`[INFO] Encrypted data: `, encrypted)

const decrypted = decrypt(encrypted, key)

console.log(`[INFO] Decrypted data: `, decrypted)

/**
 * Perform one time pad encryption
 * 
 * @param {string} string Data to be encrypted
 * @param {string} key Key for encryption
 * @returns {(0|1)[]} Encrypted data, as an array of 0's and 1's
 */
function encrypt(string, key) {
	// Transform all inputs into bits
	const string_bits = utils.stringToBits(string)
	const key_bits = utils.stringToBits(key)

	// Storage for the result
	let encrypted = []

	// Iterate all data bits
	for (let i = 0; i < string_bits.length; i++) {
		// By computing modulo find what key bit to use
		const key_bit_index = i % key_bits.length
		const key_bit = key_bits[key_bit_index]

		// Perform XOR bit operation
		const encrypted_bit = string_bits[i] ^ key_bit

		// Add to result storage
		encrypted.push(encrypted_bit)
	}

	// All done, return the result
	return encrypted
}

/**
 * Perform one time pad decryption
 * @param {(0|1)[]} encrypted_bits Encrypted data, as an array of 0's and 1's
 * @param {string} key Key for en/decryption
 * @returns {string} Data decrypted
 */
function decrypt(encrypted_bits, key) {
	// Transform all inputs into bits
	const key_bits = utils.stringToBits(key)

	// Storage for the result
	let decrypted_bits = []

	// Iterate all data bits
	for (let i = 0; i < encrypted_bits.length; i++) {
		// By computing modulo find what key bit to use
		const key_bit_index = i % key_bits.length
		const key_bit = key_bits[key_bit_index]

		// Perform XOR bit operation
		const decrypted_bit = encrypted_bits[i] ^ key_bit

		// Add to result storage
		decrypted_bits.push(decrypted_bit)
	}

	// Transform array of bits into a string
	const decrypted_string = utils.bitsToString(decrypted_bits)

	// Return the result
	return decrypted_string
}

// EOF