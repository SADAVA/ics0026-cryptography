
// S.

console.log(`
[UNIVERSITY] TalTech 2020/2021 Spring semester
[AUTHOR] Aleksander Tkatšenko, Uni-ID: 192901IVSB

[DESCRIPTION] Implementation of Vigenère Cipher.
Just set the value of variables 'string' and 'key' and see the magic.
Alternatively you can use command line arguments to pass those variables:
> node vigenere_ciper.js "string variable" "key variable"
* Variables are transformed into lower case letters to simplify code.
* Non English letters are not encrypted/decrypted.

[NOTE] Tested Node version: v12.20.1 \n`)

"use strict"

// Special variable
const CharCodePrefix = 97

// [NOTE] We will limit our data to all lower case to simplify code
//        There is absolutely no need to overcomplicate simple things
const string = (process.argv[2] || "Hello world").toLowerCase()
const key = (process.argv[3] || "abc").toLowerCase()

console.log(`[INFO] Selected string: ${string}, key: ${key}`)
console.log(`[INFO] Lengths: string: ${string.length} bytes, key: ${key.length} bytes`)

const encrypted = encrypt(string, key)

console.log(`[INFO] Encrypted data: `, encrypted)

const decrypted = decrypt(encrypted, key)

console.log(`[INFO] Decrypted data: `, decrypted)

/**
 * Encrypts string with key using Vigenère Cipher
 * 
 * @param {string} string Input data for encryption
 * @param {string} key Encryption key
 * @returns {string}
 */
function encrypt(string, key) {
	// Separate data into chars
	const string_chars = string.split('')
	const key_chars = key.split('')

	// Result storage
	let encrypted_chars = []

	// Iterate all chars individually
	for (let i = 0; i < string_chars.length; i++) {
		// By computing modulo find what key char to use
		const key_char_index = i % key_chars.length
		const key_char = key_chars[key_char_index]

		// Encrypt using Vigenère table
		const char_to_be_encrypted = string_chars[i]
		const encrypted_char = encrypt_char(char_to_be_encrypted, key_char)

		// Add to the result storage
		encrypted_chars.push(encrypted_char)
	}

	// Join all encrypted chars into a single whole
	const encrypted_string = encrypted_chars.join('')

	// All done, return
	return encrypted_string
}

/**
 * Decrypts string
 * 
 * @param {string} string Data to be decrypted
 * @param {string} key Key for decryption
 * @returns {string} Result
 */
function decrypt(string, key) {
	// Separate data into chars
	const string_chars = string.split('')
	const key_chars = key.split('')

	// Result storage
	let decrypted_chars = []

	// Iterate all chars individually
	for (let i = 0; i < string_chars.length; i++) {
		// By computing modulo find what key char to use
		const key_char_index = i % key_chars.length
		const key_char = key_chars[key_char_index]

		// Decrypt using Vigenère table
		const char_to_be_decrypted = string_chars[i]
		const decrypted_char = decrypt_char(char_to_be_decrypted, key_char)

		// Add to the result storage
		decrypted_chars.push(decrypted_char)
	}

	// Join all encrypted chars into a single whole
	const decrypted_string = decrypted_chars.join('')

	// All done, return
	return decrypted_string
}

/**
 * Vigenere encrypt char
 * 
 * @param {string} data_char First Char lower case english letter
 * @param {string} key_char Seconds Char lower case english letter
 * @returns {string} Result Char from Vigere table
 */
function encrypt_char(data_char, key_char) {
	// Transform char to number
	const data_int = data_char.charCodeAt(0) - CharCodePrefix
	const key_int = key_char.charCodeAt(0) - CharCodePrefix

	// Ensure all inputs are in valid range, ie lower case and is an eglish letter
	// if not return the data char as is
	if (0 > data_int || data_int > 26 ||
		0 > key_int || key_int > 26) return data_char

	// Vigenere encryption
	const result_int = (data_int + key_int) % 26
	const result_char = String.fromCharCode(result_int + CharCodePrefix)

	return result_char
}

/**
 * Vigenere decrypt char
 * 
 * @param {string} data_char First Char lower case english letter
 * @param {string} key_char Seconds Char lower case english letter
 * @returns {string} Result Char from Vigere table
 */
function decrypt_char(data_char, key_char) {
	// Transform char to number
	const data_int = data_char.charCodeAt(0) - CharCodePrefix
	const key_int = key_char.charCodeAt(0) - CharCodePrefix

	// Ensure all inputs are in valid range, ie lower case and is an eglish letter
	// if not return the data char as is
	if (0 > data_int || data_int > 26 ||
		0 > key_int || key_int > 26) return data_char

	// Vigenere decryption
	const result_int = (data_int - key_int + 26) % 26
	const result_char = String.fromCharCode(result_int + CharCodePrefix)

	return result_char
}

// EOF