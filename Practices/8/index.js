
// S.
// Title: Implement 3DES using any programming Language
// Author: Aleksander Thatsenko
// University: TalTech
// Date: 25th of March, 2021

/**************************************************
 *                 DESCRIPTION                    *
 **************************************************
 * Hello,                                         *
 *                                                *
 * This is an implementation of 3DES!             *
 * It was fun, yet very useful experience in my   *
 * opinion.                                       *
 *                                                *
 * Right now, you can only encrypt only one block *
 * at the time, 64 bits. I did not bother to      *
 * implement padding or anything because this is  *
 * not the point of this exercise.                *
 *                                                *
 * The code is split into parts:                  *
 *  - DES TABLES                                  *
 *  - CRYPTOGRAPHY UTILITIES                      *
 *  - NON-CRYPTOGRAPHY UTILITIES                  *
 *  - MAIN FUNCTION                               *
 *                                                *
 * Main function starts at line: 422              *
 *                                                *
 * This script requiers no dependecies.           *
 * Tested version of Node.js: v14.15.5            *
 * Actually it can run on any version I think.    *
 *                                                *
 * Good luck.                                     *
 **************************************************/

'use strict'

/**************************************************
 *                 DES TABLES                     *
 **************************************************/

const INITIAL_PERMUTATION_TABLE = [
	57, 49, 41, 33, 25, 17,  9,  1, 59, 51, 43, 35,
	27, 19, 11,  3, 61, 53, 45, 37, 29, 21, 13,  5,
	63, 55, 47, 39, 31, 23, 15,  7, 56, 48, 40, 32,
	24, 16,  8,  0, 58, 50, 42, 34, 26, 18, 10,  2,
	60, 52, 44, 36, 28, 20, 12,  4, 62, 54, 46, 38,
	30, 22, 14,  6
]

const INVERSE_INITIAL_PERMUTATION_TABLE = [
	39,  7, 47, 15, 55, 23, 63, 31, 38,  6, 46, 14,
	54, 22, 62, 30, 37,  5, 45, 13, 53, 21, 61, 29,
	36,  4, 44, 12, 52, 20, 60, 28, 35,  3, 43, 11,
	51, 19, 59, 27, 34,  2, 42, 10, 50, 18, 58, 26,
	33,  1, 41,  9, 49, 17, 57, 25, 32,  0, 40,  8,
	48, 16, 56, 24
]

const PERMUTATION_TABLE = [
	15,  6, 19, 20, 28, 11, 27, 16,  0,
	14, 22, 25,  4, 17, 30,  9,  1,  7,
	23, 13, 31, 26,  2,  8, 18, 12, 29,
	 5, 21, 10,  3, 24
]

const PC1 = [
	56, 48, 40, 32, 24, 16,  8,  0, 57, 49, 41, 33,
	25, 17,  9,  1, 58, 50, 42, 34, 26, 18, 10,  2,
	59, 51, 43, 35, 62, 54, 46, 38, 30, 22, 14,  6,
	61, 53, 45, 37, 29, 21, 13,  5, 60, 52, 44, 36,
	28, 20, 12,  4, 27, 19, 11,  3
]

const PC2 = [
	13, 16, 10, 23,  0,  4,  2, 27, 14,  5, 20,
	 9, 22, 18, 11,  3, 25,  7, 15,  6, 26, 19,
	12,  1, 40, 51, 30, 36, 46, 54, 29, 39, 50,
	44, 32, 47, 43, 48, 38, 55, 33, 52, 45, 41,
	49, 35, 28, 31
]

const round_shifts = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1]

const EXPANSION_TABLE = [
	31,  0,  1,  2,  3,  4,  3,  4,  5,  6,  7,
	 8,  7,  8,  9, 10, 11, 12, 11, 12, 13, 14,
	15, 16, 15, 16, 17, 18, 19, 20, 19, 20, 21,
	22, 23, 24, 23, 24, 25, 26, 27, 28, 27, 28,
	29, 30, 31,  0
]

const SBOX = [
	[
	[14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7],
	[0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8],
	[4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0],
	[15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13]
	],
	
	[
	[15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10],
	[3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5],
	[0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15],
	[13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9]
	],
	
	
	[
	[10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8],
	[13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1],
	[13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7],
	[1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12]
	
	],
	
	[
	[7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15],
	[13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9],
	[10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4],
	[3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14]
	],
	
	[
	[2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9],
	[14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6],
	[4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14],
	[11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3]
	],
	
	[
	[12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11],
	[10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8],
	[9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6],
	[4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13]
	
	],
	[
	[4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1],
	[13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6],
	[1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2],
	[6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12]
	],
	
	[
	[13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7],
	[1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2],
	[7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8],
	[2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11]
	]
	
]

/**************************************************
 *            CRYPTOGRAPHY UTILITIES              *
 **************************************************/

/**
 * @param {number[]} table Permutation table
 * @param {number[][]} datas Array of arrays of bits
 * @returns {number[]} Array of bits
 */
function permutate(table, ...datas) {
	const data = Array().concat(...datas)

	return table.map(index =>
		data[index])
}

/**
 * 
 * @param {number[]} bits1 
 * @param {number[]} bits2 
 * @returns {number[]}
 */
function XOR(bits1, bits2) {
	return bits1.map((bit, index) => bit ^ bits2[index])
}

/**
 * @param {number} count Number of bits to shift
 * @param {number[]} bits Array of bits
 * @returns {number[]}
 */
function shiftBits(count, bits) {
	return Array().concat(bits.slice(count), bits.slice(0, count))
}

/**
 * Generates sub keys from pirmary key
 * 
 * @param {number[]} key Primary key array of 64 bits
 * @returns {number[][]} Returns 16 sub keys, arrays of 48 bits
 */
function generateSubKeys(key) {
	const cipherkeys = []
	
	const initialPermutation = permutate(PC1, key) 
	let [ L0, R0 ] = SplitInParts(initialPermutation, 2) // In other words, split into 2 halves

	for (let i = 0; i < round_shifts.length; i++) {
		// Shift bits and swap places
		[ L0, R0 ] = [shiftBits(round_shifts[i], L0),
					  shiftBits(round_shifts[i], R0)]

		// Compute the sub key and add it to the result
		const cipherkey = permutate(PC2, R0, L0)
		cipherkeys.push(cipherkey)
	}

	return cipherkeys
}

/**
 * @param {number[]} halfBits Array of 32 bits
 * @param {number[]} cipherkey Array of 48 bits
 * @returns {number[]}
 */
function F(halfBits, cipherkey) {
	const expanded_left_half = permutate(EXPANSION_TABLE, halfBits)
	const xor_value = XOR(expanded_left_half, cipherkey)
	const bits6list = SplitInPartsOfSize(xor_value, 6) // In other words, split into parts of 6 bits

	const result = []

	for (let index = 0; index < bits6list.length; index++) {
		const bits6 = bits6list[index]

		const first_last = BinaryToInt([bits6[0], bits6[5]])
		const middle4 = BinaryToInt(bits6.slice(1, 5))
		const s = SBOX[index][first_last][middle4]
		const s_bits = IntToBinary(s)

		result.push(s_bits)
	}

	const final = permutate(PERMUTATION_TABLE, ...result)	

	return final
}

/**
 * Perform DES encryption
 * 
 * @param {number[]} plaintext Plain text data in form of an array of bits
 * @param {number[]} key Key, array of 64 bits
 * 
 * @returns {number[]} Encrypted array of bits
 */
function Encrypt(plaintext, key) {
	const subkeys = generateSubKeys(key)

	return DesCryption(plaintext, subkeys)
}

/**
 * Perform DES decryption, in simple terms, this is an inverse of encryption, nothing fancy.
 * 
 * @param {number[]} ciphertext Encrypted data, array of bits
 * @param {number[]} key Key, array of 64 bits
 * 
 * @returns {number[]} Plain text data in form of an array of bits
 */
function Decrypt(ciphertext, key) {
	const subkeys = generateSubKeys(key)
		.reverse() // Reverse the order of sub keys, that is all there to decryption

	return DesCryption(ciphertext, subkeys)
}

/**
 * Performs DES Encrypted/Decrypted
 * 
 * @param {number[]} input Encrypted/Decrypted data, array of 64 bits
 * @param {number[][]} subkeys 
 * @returns {number[]} Decrypted/Encrypted data, array of 64 bits
 */
function DesCryption(input, subkeys) {
	const first_permutated = permutate(INITIAL_PERMUTATION_TABLE, input)
	let [ L0, R0 ] = SplitInParts(first_permutated, 2) // In other words, split into 2 halves
	
	for (let i = 0; i < subkeys.length; i++) {
		// In other words: perform F function on one part of the input
		// xor it with the other part, and swap places of parts.
		[R0, L0] = [XOR(L0, F(R0, subkeys[i])), R0]
	}

	const output = permutate(INVERSE_INITIAL_PERMUTATION_TABLE, R0, L0)

	return output
}

/**************************************************
 *         NON-CRYPTOGRAPHY UTILITIES             *
 **************************************************/

const BYTE_LENGTH = 8

/**
 * @param {string} string Any normal text string
 * @returns {number[]} Returns an array of 1's and 0's
 */
function StringToBinary(string) {
	// Extract char codes from each char
	const char_codes = string.split('').map(char => char.charCodeAt(0))

	// Turn char codes into binarry arrays
	const binaries = char_codes.map(IntToBinary)

	// Concat all arrays together
	const bits = Array().concat(...binaries)

	return bits
}

/**
 * Parses an integer number into array of bits
 * 
 * @param {number} number Number in range of 0 to 255
 * @returns {number[]} Byte array of 0's and 1'
 */
function IntToBinary(number) {
	const binary_string = number.toString(2)

	// Ensure all charcodes (binary string) are exactly one byte long
	const fixed_sized = Array(BYTE_LENGTH - binary_string.length).fill('0').join('') + binary_string
	// Join all chunks alltogether and split each individually

	const string_bits = fixed_sized.split('')

	// Parse '0' and '1' into numbers 0 and 1
	const bits = string_bits.map(s => parseInt(s))

	return  bits
}

/**
 * @param {number[]} arr An array of 1's and 0's
 * @returns {string} Returns a normal text string
 */
function BinaryToString(arr) {
	const codes = []

	for (let byte = 0; byte < arr.length / BYTE_LENGTH; byte++) {
		const code = []

		for (let index = 0; index < BYTE_LENGTH; index++)
			code.push(arr[byte * BYTE_LENGTH + index])

		codes.push(code)
	}

	const codes_merged = codes.map(code => code.join(''))
	const codes_numbers = codes_merged.map(code => parseInt(code, 2))
	const chars = codes_numbers.map(code => String.fromCharCode(code))
	const string = chars.join('')

	return string
}

/**
 * Converts array of binary 0's and 1's into an integer number
 * 
 * @param {number[]} array Array of 0's and 1's
 * @returns {number} Returns integer number
 */
function BinaryToInt(array) {
	const binaryString = array.join('')
	const number = parseInt(binaryString, 2)

	return number
}

/**
 * Splits array to two equal halfes
 * 
 * @template T
 * @param {T[]} array 
 * @param {number} parts
 * @returns {T[][]}
 */
function SplitInParts(array, parts) {
	if (array.length % parts !== 0) throw new RangeError(`SplitInParts: array.length % count !== 0, actual value: ${array.length % parts}`)

	const result = []
	const size = array.length / parts

	for (let i = 0; i < parts; i++) {
		const part = array.slice(size * i, size * (i + 1))
		result.push(part)
	}

	return result
}

/**
 * 
 * @template T
 * @param {T[]} array 
 * @param {number} size 
 * @returns {T[][]}
 */
function SplitInPartsOfSize(array, size) {
	if (array.length % size !== 0) throw new RangeError(`SplitInPartsOfSize: array.length % size !== 0, actual value: ${array.length % size}`)

	const result = []
	const parts = array.length / size

	for (let i = 0; i < parts; i++) {
		const part = array.slice(size * i, size * (i + 1))
		result.push(part)
	}

	return result
}

/**************************************************
 *                 MAIN FUNCTION                  *
 **************************************************/

async function main() {
    const original = 'asdasdsd' // Block should be 64 bits, which is 8 characters.
	const plaintext = StringToBinary(original)
	const key1 = StringToBinary('S0m3K3y1') // Keys should be 64 bits, which is 8 characters.
	const key2 = StringToBinary('S0m3K3y2')
	const key3 = StringToBinary('S0m3K3y3')

	console.log('[DEBUG] Original text:       ', original)
	console.log('[DEBUG] Plain text binary:   ', plaintext.join(''))
	console.log('[DEBUG]')
	console.log('[DEBUG] Key [1] binary:      ', key1.join(''))
	console.log('[DEBUG] Key [2] binary:      ', key2.join(''))
	console.log('[DEBUG] Key [3] binary:      ', key3.join(''))
	console.log('[DEBUG]')

	const ciphertext1 = Encrypt(plaintext,   key1)
	const ciphertext2 = Decrypt(ciphertext1, key2)
	const ciphertext3 = Encrypt(ciphertext2, key3)

	console.log('[DEBUG] Ciphertext [1]:      ', ciphertext1.join(''))
	console.log('[DEBUG] Ciphertext [2]:      ', ciphertext2.join(''))
	console.log('[DEBUG] Ciphertext [3]:      ', ciphertext3.join(''))
	console.log('[DEBUG]')

	const decrypted1 = Decrypt(ciphertext3, key3)
	const decrypted2 = Encrypt(decrypted1,  key2)
	const decrypted3 = Decrypt(decrypted2,  key1)

	const decrypted_text = BinaryToString(decrypted3)

	console.log('[DEBUG] Decrypted [1]:       ', decrypted1.join(''))
	console.log('[DEBUG] Decrypted [2]:       ', decrypted2.join(''))
	console.log('[DEBUG] Decrypted [3]:       ', decrypted3.join(''))
	console.log('[DEBUG]')
	console.log('[DEBUG] Decrypted text:      ', decrypted_text)

	const match = decrypted_text === original

	console.log('[DEBUG] Original text and decrypted texts: ',
		match ? 'Perfectly match eachother.' : 'UNMATCHED, THEY ARE DIFFERENT, PLEASE INVESTIGATE!')
}

main()
    .then(() => console.log('Script is done'))
    .catch(e => console.error('Top-level error occured:', e))


// Used online resources:
// https://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm [28th of March, 2021]

// EOF