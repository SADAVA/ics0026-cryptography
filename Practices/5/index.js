
// S.
// Title: Implement ElGamal using any programming Language
// Author: Aleksander Thatsenko
// University: TalTech
// Date: 9th of March, 2021

'use strict'

// Source code: https://peterolson.github.io/BigInteger.js/BigInteger.min.js
const bigInt = require("./BigInteger.min.js") // By itself nodejs can't handle this huge numbers

const Domain = {
	p: 283,
	q: 47,
	g: 60
}

function main() {
	// Alice (or anyone)
	const BobsPublicKey = 216
	const EncodedMessage = 101

	const k = RandomK(Domain.q)
	const c1 = ComputeC1(Domain.g, Domain.p, k)
	const c2 = ComputeC2(EncodedMessage, BobsPublicKey, Domain.p, k)
	
	// Bob, receives c1 and c2
	const BobsPrivateKey = 7
	const DecipheredMessage = Decipher(c1, c2, Domain.p, BobsPrivateKey)

	// Compare the results
	if (DecipheredMessage.compare(EncodedMessage) === 0)
		console.log(`Message has been successefully enciphered and deciphered back`)

	else console.log(`ERROR Encoded message did not match deciphered message, please examine:`,
					 `EncodedMessage: ${EncodedMessage} !== DecipheredMessage: ${DecipheredMessage}`)
}


/**************************
 *      CRYPTOGRAPHY      *
 **************************/

/**
 * 
 * @param {number} c1 `c1` of ciphertext
 * @param {number} c2 `c2` of ciphertext
 * @param {number} p `p` parameter of the domain
 * @param {number} privateKey Private key
 * @returns {number} `c1(power p - privateKey - 1) * c2 mod p`
 */
function Decipher(c1, c2, p, privateKey) {
	return bigInt(c1).pow(p - privateKey - 1).times(c2).mod(p)
}

/**
 * Computes c1
 * 
 * @param {number} g `g` parameter of the domain
 * @param {number} p `p` parameter of the domain
 * @param {number} k Random `k`
 * @returns {bigInt.NativeBigInt} `g(power k) mod p`
 */
function ComputeC1(g, p, k) {
	return bigInt(g).pow(k).mod(p)
}

/**
 * 
 * @param {number} encodedMessage Encoded message to encipher
 * @param {number} publicKey Destination's pulic key
 * @param {number} p `p` parameter of the domain
 * @param {number} k Random `k`
 * @returns {number} `publicKey(power k) * encodedMessage mod p`
 */
function ComputeC2(encodedMessage, publicKey, p, k) {
	return bigInt(publicKey).pow(k).times(encodedMessage).mod(p)
}

/**
 * Choose random `k`
 * 
 * @param {number} q `q` parameter of the domain
 * @returns {number} Random number in range `[2, q - 2]`
 */
function RandomK(q) {
	return 2 + Math.floor(Math.random() * (q - 4))
}

main()

// EOF