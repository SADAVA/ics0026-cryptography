
// S.
// Title: Implement MD5, SHA-1 and SHA-256, salt and benchmarking
// Author: Aleksander Thatsenko
// University: TalTech
// Date: 2nd of April, 2021

/**************************************************
 *                 DESCRIPTION                    *
 **************************************************
 * In this exercise I have implemented 3 great    *
 * hashing algorithms, namely: MD5, SHA-1 and     *
 * SHA-256. I have followed IETF RFCs standards.  *
 *                                                *
 * Besides that I have also made a benchmark, to  *
 * find out how much time each hashing takes.     *
 * Performance of my implementations may not be   *
 * great, but overall ration of speeds should be  *
 * more or less the same as with real world       *
 * implementations.                               *
 *                                                *
 * Another point of this exercise is to use salt. *
 * On it's own, salt is not anything super magic. *
 * By using small salt, we ensure that our hashes *
 * are not yet known, ie rainbow table attacks.   *
 * And by using large salt, we can even protect   *
 * against brute force attacks.                   *
 * So in this exercise I make a small benchmark   *
 * of using salts of small and big sizes with     *
 * high precision.                                * 
 *                                                *
 * Table of contents:                             *
 *  -  110 - NON-CRYPTOGRAPHY UTILITIES           *
 *  -  411 - MD5                                  *
 *  -  692 - SHA-1                                *
 *  -  918 - SHA-256                              *
 *  - 1204 - MAIN FUNCTION                        *
 *  - 1224 - BENCHMARK                            *
 *  - 1349 - TESTS: general functionality tests   *
 *  - 1568 - End of file                          *
 *                                                *
 * Notes:                                         *
 *  - For code portability, all hash functions do *
 *    not use each others functions except utils. *
 *  - My MD5 implementation does not match with   *
 *    official test cases, but it is no big deal  *
 *    for the sake of this exercise.              *
 *    Probably I have some errors in endianess.   *
 *                                                *
 * Performance note: all implementations are for  *
 * educational purpose only, all data is          *
 * respresented as arrays of integers 0's and 1's *
 * and all binary operations are implemented as a *
 * bunch of 'if-else' statements, and those       *
 * binary operations work on one bit at the time. *
 *                                                *
 * How much time it takes to run 1000 cycles:     *
 *  [ Hash:    avg seconds (deviation) ]          *
 *  - MD5:      43 seconds (+/- 0 seconds)        *
 *  - SHA1:     74 seconds (+/- 1 seconds)        *
 *  - SHA256:  152 seconds (+/- 0 seconds)        *
 *                                                *
 * Salt Benchmarking results (run hash once):     *
 *  [No salt]                                     *
 *  - MD5:     5.39 milliseconds                  *
 *  - SHA1:    8.10 milliseconds                  *
 *  - SHA256: 17.54 milliseconds                  *
 *  [Small salt]                                  *
 *  - MD5:     5.41 milliseconds                  *
 *  - SHA1:    8.45 milliseconds                  *
 *  - SHA256: 18.46 milliseconds                  *
 *  [Large salt]                                  *
 *  - MD5:     9.88 milliseconds                  *
 *  - SHA1:   16.82 milliseconds                  *
 *  - SHA256: 35.37 milliseconds                  *
 *                                                *
 * Benchmarking Configuration:                    *
 *  - Hardware:                                   *
 *      Intel Core i3-3120M (3M Cache, 2.50 GHz)  *
 *  - Cycles: 1000                                *
 *  - Salt cycles: 100                            *
 *  - Total runs: 4                               *
 *  - StartingSubject: Hello Crypto               *
 *                                                *
 * It clearly shows that using small salt         *
 * does not effect performance, but on the other  *
 * hand, large salt increases the amount of       *
 * BLOCKs that need to be proccessed by the hash  *
 * algorithm which results in doubling the time.  *
 *                                                *
 * That is pretty much it. This was a nice        *
 * exercise, I had a lot of fun doing it.         *
 * I myself implemented, wrote each line, of such *
 * honored hashing algorithms.                    *
 *                                                *
 * And I am here by calling it a day!             *
 * Assignment 2 is now finished.                  *
 *     TimeStamp: 8pm on 7th of April, 2021       *
 *                                                *
 * Sources:                                       *
 *  - https://tools.ietf.org/html/rfc1321 MD5     *
 *  - https://tools.ietf.org/html/rfc3174 SHA-1   *
 *  - https://tools.ietf.org/html/rfc4634 SHA-256 *
 *                                                *
 * Thank you                                      *
 *                                                *
 **************************************************/

/**************************************************
 *         NON-CRYPTOGRAPHY UTILITIES             *
 **************************************************/

const CHAR = 4
const BYTE = 8
const WORD = 16
const DWORD = 32
const QWORD = 64
const BLOCK = 512

/**
 * @param {string} hex Hex
 * @param {number} length
 */
function HexToBinary(hex, length) {
	const number = parseInt(hex, 16)
	return IntToBinary(number, length)
}

/**
 * Converts a decimal number to a binary sequence, overflow will be cut off
 * 
 * @param {number} number A normal decimal number 
 * @param {number} length Number of bits, BYTE, WORD, DWORD, QWORD
 * 
 * @returns {number[]} Array of bits in Little endian
 */
function IntToBinary(number, length) {
	if (length === undefined) length = BYTE

	const binary_string = number.toString(2)

	const string_bits = binary_string.split('')

	// Parse '0' and '1' into numbers 0 and 1, and turn it into little endian
	let bits = string_bits.map(s => parseInt(s))

	if (bits.length > length)
		bits = bits.slice(bits.length - length)
	
	else if (bits.length !== length)
		bits = Array(length - bits.length).fill(0).concat(bits)

	return bits
}

/**
 * Convers binary sequence to decimal number
 * 
 * @param {number[]} array Little endian array of bits
 * 
 * @returns {number}
 */
function BinaryToInt(array) {
	return parseInt(array.reverse().join(''), 2)
}

/**
 * Convers a string to binary sequnce
 * 
 * @param {string} string Any normal text string
 * 
 * @returns {number[]} Returns an array of 1's and 0's, Little endianess
 */
function StringToBinary(string) {
	// Extract char codes from each char
	const char_codes = string.split('').map(char => char.charCodeAt(0))

	// Turn char codes into binarry arrays
	const binaries = char_codes.map(code => IntToBinary(code))

	// Concat all arrays together
	const bits = Array().concat(...binaries)

	return bits
}

/**
 * Convers binary sequence into HEXadecimal representation
 * 
 * @param {number[]} arr Array of bits, should be of minimum modulus CHAR
 * 
 * @throws {Error} if modulus is incorrect
 * 
 * @returns {string} String of HEX
 */
function BinaryToHex(arr) {
	if (arr.length % CHAR)
		throw new Error(`Array should be of modulus ${CHAR}`)

	let result = []

	// Iterate chars
	for (let i = 0; i < arr.length / CHAR; i++) {
		// Current char
		const char = arr.slice(CHAR * i, CHAR * (i + 1))

		// Convert char to number
		const number = parseInt(char.join(''), 2)

		// Conver number to hex and store it
		result.push(number.toString(16))
	}

	// Little endianess and join pieces all together
	return result.join('')
}

/**
 * Sums up binary sequences
 * 
 * @param {number[]} a
 * @param {number[]} b
 * @param {number[][]} extras
 * 
 * @returns {number[]} Array of bits, Little endianess, length is determined by the longest input
 */
function BinarySum(a, b, ...extras) {
	if (b.length > a.length)
		[a, b] = [b, a]
	a = a.reverse()
	b = b.reverse()
	const result = Array(a.length).fill(0)
	
	let increment = false
	
	for (let i = 0; i < a.length; i++) {
		if (i > b.length - 1) {
			if (increment === true) {
				result[i] = 1

				if (a[i] === 0) {
					increment = false
				}
			} else {
				result[i] = a[i]
			}

			continue
		}

		if (a[i] !== 0 || b[i] !== 0) {
			if (a[i] === 1 && b[i] === 1) {
				if (increment === true) {
					result[i] = 1
				} else increment = true
			} else {
				if (increment === true) {
					result[i] = 0
				} else {
					result[i] = 1
				}
			}

			continue
		}

		if (increment === true) {
			if (result[i] === 0) {
				result[i] = 1
				increment = false
			}
		}
	}

	a = a.reverse()
	b = b.reverse()

	if (extras.length == 0) {
		return result.reverse()
	} else {
		return BinarySum(result.reverse(), ...extras)
	}
}

/**
 * 
 * @param {number} bit Bit value, 0 or 1
 * @returns {number} Not result, if input was 1 it will be 0, and otherwise.
 */
function not(bit) {
	if (bit === 0)
		return 1
	else if (bit === 1)
		return 0
	else throw new Error(`Invalid bit value: ${bit}`)
}

/**
 * Binary operation: AND
 * 
 * Operands must be of the same length
 * 
 * @param {number[]} a
 * @param {number[]} b
 * 
 * @returns {number[]}
 */
function AND(a, b) {
	if (a.length !== b.length)
		throw new Error(`Operands 'a' and 'b' must be of the same length: a.length: ${a.length} !== b.length: ${b.length}`)

	return a.map((_, i) => {
		a_bit = a[i]
		b_bit = b[i]

		return (a_bit === 1 && b_bit === 1) ? 1 : 0
	})
}

/**
 * Binary operation: OR
 * 
 * Operands must be of the same length
 * 
 * @param {number[]} a
 * @param {number[]} b
 * 
 * @returns {number[]}
 */
function OR(a, b) {
	if (a.length !== b.length)
		throw new Error(`Operands 'a' and 'b' must be of the same length: a.length: ${a.length} !== b.length: ${b.length}`)

	return a.map((_, i) => {
		a_bit = a[i]
		b_bit = b[i]

		return (a_bit === 1 || b_bit === 1) ? 1 : 0
	})
}

/**
 * Binary operation: XOR
 * 
 * Operands must be of the same length
 * 
 * @param {number[]} a
 * @param {number[]} b
 * 
 * @returns {number[]}
 */
function XOR(a, b) {
	if (a.length !== b.length)
		throw new Error(`Operands 'a' and 'b' must be of the same length: a.length: ${a.length} !== b.length: ${b.length}`)

	return a.map((_, i) => {
		a_bit = a[i]
		b_bit = b[i]

		return (a_bit === b_bit) ? 0 : 1
	})
}

/**
 * Binary operation: NOT
 * 
 * @param {number[]} a
 * 
 * @returns {number[]}
 */
function NOT(a) {
	return a.map((_, i) => {
		a_bit = a[i]

		return (a_bit === 1) ? 0 : 1
	})
}

/**
 * @param {number} count Number of bits to rotate
 * @param {number[]} bits Array of bits
 * @returns {number[]}
 */
function RotateBits(count, bits) {
	count = count % bits.length
	return Array().concat(bits.slice(count), bits.slice(0, count))
}

/**
 * @param {number} count Number of bits to shift
 * @param {number[]} bits Array of bits
 * @returns {number[]}
 */
function ShiftBits(count, bits) {
	if (count > bits.length)
		return Array(bits.length).fill(0)

	if (count === 0)
		return bits
	
	if (count > 0) {
		const result = Array().concat(bits, Array(count).fill(0))
		return result.slice(count)
	} else {
		const result = Array().concat(Array(count * -1).fill(0), bits)
		return result.slice(0, bits.length)
	}
}

/**************************************************
 *                     MD5                        *
 **************************************************/

// Some magic T table for RFC1321 [3.4] Step 4.
// 64-element table T[1 ... 64] the integer part of 4294967296 times abs(sin(i)), where i is in radians.
const T_raw_hex = [
	'D76AA478', 'E8C7B756', '242070DB', 'C1BDCEEE',
	'F57COFAF', '4787C62A', 'A8304613', 'FD469501',
	'698098D8', '8B44F7AF', 'FFFF5BB1', '895CD7BE',
	'6B901122', 'FD987193', 'A679438E', '49B40821',
	'F61E2562', 'C040B340', '265E5A51', 'E9B6C7AA',
	'D62F105D', '02441453', 'D8A1E681', 'E7D3FBC8',
	'21E1CDE6', 'C33707D6', 'F4D50D87', '455A14ED',
	'A9E3E905', 'FCEFA3F8', '676F02D9', '8D2A4C8A',
	'FFFA3942', '8771F681', '699D6122', 'FDE5380C',
	'A4BEEA44', '4BDECFA9', 'F6BB4B60', 'BEBFBC70',
	'289B7EC6', 'EAA127FA', 'D4EF3085', '04881D05',
	'D9D4D039', 'E6DB99E5', '1FA27CF8', 'C4AC5665',
	'F4292244', '432AFF97', 'AB9423A7', 'FC93A039',
	'655B59C3', '8F0CCC92', 'FFEFF47D', '85845DD1',
	'6FA87E4F', 'FE2CE6E0', 'A3014314', '4E0811A1',
	'F7537E82', 'BD3AF235', '2AD7D2BB', 'EB86D391'
]

const T = T_raw_hex.map(v => HexToBinary(v, DWORD))

/**
 * RFC1321 [3.1] Step 1. Append Padding Bits
 * 
 * The message is "padded" (extended) so that its length (in bits) is
 * congruent to 448, modulo 512.
 * 
 * @param {number[]} data Input data, array of bits of any length
 * 
 * @returns {number[]} Padded input data, array of bits congruent to 448, modulo 512.
 */
function md5pad(data) {
	const offset = BLOCK - data.length % BLOCK

	// Padding is always performed, even if the length of the message is
	// already congruent to 448, modulo 512.
	let padding_length = offset - 64
	if (padding_length < 1) padding_length = BLOCK + padding_length

	const zeros_length = padding_length - 1
	const zeros = Array(zeros_length).fill(0)

	// In all, at least one bit and at most 512 bits are appended.
	// And the output must be congruent to 448, modulo 512.

	// return zeros.concat(1).concat(data)
	return [...data, 1, ...zeros]
}

/**
 * RFC1321 [3.2] Step 2. Append Length
 * 
 * A 64-bit representation of b is appended to the result
 * of the previous step.
 * 
 * @param {number[]} padded_data 
 * @param {number} length the length of the message before the padding bits were added
 * 
 * @returns {number[]} Array of bits, whos length is a multiple of 512 bits.
 */
function md5length(padded_data, length) {
	const length_bits = IntToBinary(length, QWORD)

	// return padded_data.concat(length_bits)
	return [...padded_data, ...length_bits]
}

/**
 * RFC1321 [3.3] Step 3. Initialize MD Buffer
 * 
 * Generates A four-word buffer (A, B, C, D) which is used
 * to compute the message digest.
 * 
 * @returns {number[][]} 4 arrays of bits
 */
function md5buffers() {
	return [
		HexToBinary('67452301', DWORD), // Word A
		HexToBinary('EFCDAB89', DWORD), // Word B
		HexToBinary('98BADCFE', DWORD), // Word C
		HexToBinary('10325476', DWORD)  // Word D
	]
}

/**
 * RFC1321 [3.4] Step 4. md5 function F
 * 
 * In each bit position F acts as a conditional: if X then Y else Z.
 * F(x, y, z) => (((x) & (y)) | ((~x) & (z)))
 * 
 * @param {number[]} X Array of 32 bits
 * @param {number[]} Y Array of 32 bits
 * @param {number[]} Z Array of 32 bits
 * 
 * @returns {number[]} Array of 32 bits
 */
function md5functionF(X, Y, Z) {
	const result = []

	for (let index = 0; index < DWORD; index++) {
		if (X[index] === 1)
			result.push(Y[index])
		else
			result.push(Z[index])
	}

	return result
}

/**
 * RFC1321 [3.4] Step 4. md5 function G
 * 
 * G(x, y, z) => (((x) & (z)) | ((y) & (~z)))
 * 
 * @param {number[]} X Array of 32 bits
 * @param {number[]} Y Array of 32 bits
 * @param {number[]} Z Array of 32 bits
 * 
 * @returns {number[]} Array of 32 bits
 */
function md5functionG(X, Y, Z) {
	return Array(DWORD).fill(null).map((_, i) =>
		(X[i] & Z[i]) | (Y[i] & not(Z[i])))
}

/**
 * RFC1321 [3.4] Step 4. md5 function H
 * 
 * H(x, y, z) => ((x) ^ (y) ^ (z))
 * 
 * @param {number[]} X Array of 32 bits
 * @param {number[]} Y Array of 32 bits
 * @param {number[]} Z Array of 32 bits
 * 
 * @returns {number[]} Array of 32 bits
 */
function md5functionH(X, Y, Z) {
	const result = []

	for (let index = 0; index < DWORD; index++) {
		result.push(X[index] ^ Y[index] ^ Z[index])
	}

	return result
}

/**
 * RFC1321 [3.4] Step 4. md5 function I
 * 
 * I(x, y, z) => ((y) ^ ((x) | (~z)))
 * 
 * @param {number[]} X Array of 32 bits
 * @param {number[]} Y Array of 32 bits
 * @param {number[]} Z Array of 32 bits
 * 
 * @returns {number[]} Array of 32 bits
 */
function md5functionI(X, Y, Z) {
	const result = []

	for (let index = 0; index < DWORD; index++) {
		result.push(Y[index] ^ (X[index] || not(Z[index])))
	}

	return result
}

/**
 * @param {number[]} data Input data, array of bits
 * @returns {string} HEX representation of md5 result
 */
function md5hex(data) {
	return BinaryToHex(md5(data))
}

/**
 * @param {number[]} a md5 buffer A
 * @param {number[]} b md5 buffer B
 * @param {number[]} c md5 buffer C
 * @param {number[]} d md5 buffer D
 * @param {number}   x X value
 * @param {number}   s shift
 * @param {number}   i index at T
 * @param {function} func md5 function (F, G, H, I)
 * 
 * @returns {number[]} Processed A
 */
function md5process(a, b, c, d, x, s, i, func) {
	a = BinarySum(a,func(b, c, d), x, T[i - 1])
	a = RotateBits(s, a)
	a = BinarySum(a, b)

	return a
}

/**
 * @param {number[]} data Input data, array of bits
 * @returns {number[]} md5 output, array of bits
 */
function md5(data) {
	const padded = md5pad(data)
	const lengthed = md5length(padded, data.length)

	let [A, B, C, D] = md5buffers()

	for (let i = 0; i < lengthed.length / BLOCK; i++) {
		// Process that block and get back buffers, with a slice of current block
		[A, B, C, D] = md5processBlock(A, B, C, D, lengthed.slice(BLOCK * i, BLOCK * (i + 1)))
	}

	return Array().concat(A, B, C, D)
}

function md5processBlock(A, B, C, D, block) {
	let dwords = []

	for (let i = 0; i < BLOCK / DWORD; i++)
		dwords.push(block.slice(DWORD * i, DWORD * (i + 1)))

	const AA = [...A]
	const BB = [...B]
	const CC = [...C]
	const DD = [...D]
	const X = dwords

	// Functions shortcuts
	const F = md5functionF
	const G = md5functionG
	const H = md5functionH
	const I = md5functionI

	const ABCD = (k, s, i, func) =>
		A = md5process(A, B, C, D, X[k], s, i, func)

	const DABC = (k, s, i, func) =>
		D = md5process(D, A, B, C, X[k], s, i, func)

	const CDAB = (k, s, i, func) =>
		C = md5process(C, D, A, B, X[k], s, i, func)

	const BCDA = (k, s, i, func) =>
		B = md5process(B, C, D, A, X[k], s, i, func)

	/* Round 1. */
	ABCD(  0,   7,   1,  F);  DABC(  1,  12,   2,  F);  CDAB(  2,  17,   3,  F);  BCDA(  3,  22,   4,  F)
	ABCD(  4,   7,   5,  F);  DABC(  5,  12,   6,  F);  CDAB(  6,  17,   7,  F);  BCDA(  7,  22,   8,  F)
	ABCD(  8,   7,   9,  F);  DABC(  9,  12,  10,  F);  CDAB( 10,  17,  11,  F);  BCDA( 11,  22,  12,  F)
	ABCD( 12,   7,  13,  F);  DABC( 13,  12,  14,  F);  CDAB( 14,  17,  15,  F);  BCDA( 15,  22,  16,  F)

	/* Round 2. */
	ABCD(  1,   5,  17,  G);  DABC(  6,   9,  18,  G);  CDAB( 11,  14,  19,  G);  BCDA(  0,  20,  20,  G)
	ABCD(  5,   5,  21,  G);  DABC( 10,   9,  22,  G);  CDAB( 15,  14,  23,  G);  BCDA(  4,  20,  24,  G)
	ABCD(  9,   5,  25,  G);  DABC( 14,   9,  26,  G);  CDAB(  3,  14,  27,  G);  BCDA(  8,  20,  28,  G)
	ABCD( 13,   5,  29,  G);  DABC(  2,   9,  30,  G);  CDAB(  7,  14,  31,  G);  BCDA( 12,  20,  32,  G)

	/* Round 3. */
	ABCD(  5,   4,  33,  H);  DABC(  8,  11,  34,  H);  CDAB( 11,  16,  35,  H);  BCDA( 14,  23,  36,  H)
	ABCD(  1,   4,  37,  H);  DABC(  4,  11,  38,  H);  CDAB(  7,  16,  39,  H);  BCDA( 10,  23,  40,  H)
	ABCD( 13,   4,  41,  H);  DABC(  0,  11,  42,  H);  CDAB(  3,  16,  43,  H);  BCDA(  6,  23,  44,  H)
	ABCD(  9,   4,  45,  H);  DABC( 12,  11,  46,  H);  CDAB( 15,  16,  47,  H);  BCDA(  2,  23,  48,  H)

	/* Round 4. */
	ABCD(  0,   6,  49,  I);  DABC(  7,  10,  50,  I);  CDAB( 14,  15,  51,  I);  BCDA(  5,  21,  52,  I)
	ABCD( 12,   6,  53,  I);  DABC(  3,  10,  54,  I);  CDAB( 10,  15,  55,  I);  BCDA(  1,  21,  56,  I)
	ABCD(  8,   6,  57,  I);  DABC( 15,  10,  58,  I);  CDAB(  6,  15,  59,  I);  BCDA( 13,  21,  60,  I)
	ABCD(  4,   6,  61,  I);  DABC( 11,  10,  62,  I);  CDAB(  2,  15,  63,  I);  BCDA(  9,  21,  64,  I)

	A = BinarySum(A, AA)
	B = BinarySum(B, BB)
	C = BinarySum(C, CC)
	D = BinarySum(D, DD)

	return [A, B, C, D]
}

/**************************************************
 *                    SHA-1                       *
 **************************************************/

/**
 * RFC3174 [4] Message Padding
 * 
 * As a summary, a "1" followed by m "0"s followed by a 64-
 * bit integer are appended to the end of the message to produce a
 * padded message of length 512 * n.
 * 
 * @param {number[]} data Message, array of bits, any length
 * 
 * @returns {number[]} Padded message, array of bits, length of modulo 512
 */
function sha1pad(data) {
	// How much bits are missing to complement 512 modulo,
	// 64 bits is for data length
	let offset = BLOCK - data.length % BLOCK - 64

	if (offset < 1)
		offset = BLOCK + offset

	// 1 followed by 0's to fit the length of offset
	const pad = [1, ...Array(offset - 1).fill(0)]

	// 64bit integer of data's length
	const length = IntToBinary(data.length, QWORD, false)
	
	// Put pieces together
	const result = Array().concat(data, pad, length)

	// Done
	return result
}

/**
 * Splits data into BLOCK
 * 
 * @param {number[]} data Padded data
 * @returns {number[][]} BLOCKS
 */
function sha1M(data) {
	if (data.length % BLOCK !== 0)
		throw new Error(`parameter data should be of modulus BLOCK: ${BLOCK}, but modulus leftover ${data.length % BLOCK} given`)

	const result = []

	for (let i = 0; i < data.length / BLOCK; i++)
		result.push(data.slice(BLOCK * i, BLOCK * (i + 1)))

	return result
}

/**
 * Splits BLOCK into DWORDs
 * 
 * @param {number[]} block BLOCK
 * @returns {number[][]} DWORDs
 */
function sha1W(block) {
	if (block.length !== BLOCK)
		throw new Error(`Parameter block should be of length BLOCK: ${BLOCK}, gut length: ${block.length} was given`)

	const result = []

	for (let i = 0; i < block.length / DWORD; i++)
		result.push(block.slice(DWORD * i, DWORD * (i + 1)))

	return result
}

/**
 * Pad data and split it into BLOCKS
 * @param {number[]} data 
 * @returns {number[][]} BLOCKS
 */
function sha1padIntoM(data) {
	data = sha1pad(data)
	const M = sha1M(data)
	return M
}

/**
 * RFC3174 [5] Functions and Constants Used
 * 
 * A sequence of logical functions f(0), f(1),..., f(79) is used in
 * SHA-1.  Each f(t), 0 <= t <= 79, operates on three 32-bit words B, C,
 * D and produces a 32-bit word as output.
 * 
 * @param {number} t number in range of 0 to 79
 * @param {number[]} B DWORD
 * @param {number[]} C DWORD
 * @param {number[]} D DWORD
 * 
 * @returns {number[]}
 */
function sha1f(t, B, C, D) {
	if (t < 0 || t > 79)
		throw new Error(`invalid parameter 't': ${t}, it should be in range of 0 to 79`)

	if (B.length !== DWORD || C.length !== DWORD || D.length !== DWORD)
		throw new Error(`All buffers (B, C, D) should be of length DWORD: ${DWORD}`)

	// f(t;B,C,D) = (B AND C) OR ((NOT B) AND D)         ( 0 <= t <= 19)
	if (t < 20)
		return OR(AND(B, C), AND(NOT(B), D))

	// f(t;B,C,D) = B XOR C XOR D                        (20 <= t <= 39)
	if (t < 40)
		return XOR(D, XOR(B, C))

	// f(t;B,C,D) = (B AND C) OR (B AND D) OR (C AND D)  (40 <= t <= 59)
	if (t < 60)
		return OR(OR(AND(B, C), AND(B, D)), AND(C, D))

	// f(t;B,C,D) = B XOR C XOR D                        (60 <= t <= 79).
	if (t < 80)
		return XOR(XOR(B, C), D)
}

/**
 * @param {number} quantity Number of buffers
 * @param {number} size Size, BYTE, WORD, DWORD, ...etc
 * @param {0|1} [defaultBit=0] Default bit, by default 0
 * 
 * @returns {number[][]}
 */
function sha1createBuffers(quantity, size, defaultBit = 0) {
	return Array(quantity).fill(null).map(() => Array(size).fill(defaultBit))
}

/**
 * Creates sha-1 H buffer
 * 
 * @returns {number[]}
 */
function sha1createHBuffer() {
	return [
		HexToBinary('67452301', DWORD),
		HexToBinary('EFCDAB89', DWORD),
		HexToBinary('98BADCFE', DWORD),
		HexToBinary('10325476', DWORD),
		HexToBinary('C3D2E1F0', DWORD)
	]
}

/**
 * Creates sha-1 K buffer
 * 
 * @returns {number[]}
 */
function sha1createKBuffer() {
	return [
		HexToBinary('5A827999', DWORD),
		HexToBinary('6ED9EBA1', DWORD),
		HexToBinary('8F1BBCDC', DWORD),
		HexToBinary('CA62C1D6', DWORD),
	]
}

/**
 * @param {number[]} data 
 * @returns {number[]} 5 DWORDs
 */
function sha1(data) {
	// Buffers of DWORDS
	let [A, B, C, D, E]      = sha1createBuffers( 5, DWORD)
	let [H0, H1, H2, H3, H4] = sha1createHBuffer()
	const W                  = sha1createBuffers(80, DWORD)
	const K                  = sha1createKBuffer()
	let [TEMP]               = sha1createBuffers( 1, DWORD)

	// Pad data, and split into blocks
	const M = sha1padIntoM(data)

	for (let m = 0; m < M.length; m++) {
		// Current block
		const Mi = M[m]

		// a. Divide M(i) into 16 words W(0), W(1), ... , W(15), where W(0)
		//    is the left-most word.
		const W16 = sha1W(Mi)

		for (let w = 0; w < W16.length; w++)
			W[w] = W16[w]

		// b. For t = 16 to 79 let
        //    W(t) = S^1(W(t-3) XOR W(t-8) XOR W(t-14) XOR W(t-16)).
		for (let t = 16; t < 80; t++) {
			W[t] = RotateBits(1, XOR(XOR(XOR(W[t - 3], W[t - 8]), W[t - 14]), W[t - 16]))
		}

		// c. Let A = H0, B = H1, C = H2, D = H3, E = H4.
		A = H0; B = H1; C = H2; D = H3; E = H4

		// d. For t = 0 to 79 do
		//    TEMP = S^5(A) + f(t;B,C,D) + E + W(t) + K(t);
		//    E = D;  D = C;  C = S^30(B);  B = A; A = TEMP;
		for (let t = 0; t < 80; t++) {
			TEMP = BinarySum(RotateBits(5, A), sha1f(t, B, C, D), E, W[t], K[Math.floor(t / 20)])

			E = D; D = C; C = RotateBits(30, B); B = A; A = TEMP
		}

		// e. Let H0 = H0 + A, H1 = H1 + B, H2 = H2 + C, 
		//    H3 = H3 + D, H4 = H4 + E.
		H0 = BinarySum(H0, A)
		H1 = BinarySum(H1, B)
		H2 = BinarySum(H2, C)
		H3 = BinarySum(H3, D)
		H4 = BinarySum(H4, E)
	}

	return [H0, H1, H2, H3, H4]
}


/**
 * @param {number[]} data Input data, array of bits
 * @returns {string} HEX representation of sha1 result
 */
function sha1hex(data) {
	return BinaryToHex(sha1(data).flat())
}

/**************************************************
 *                   SHA-256                      *
 **************************************************/

/**
 * RFC4634 [5.1] SHA-256
 * 
 * SHA-256 use the sequence of sixty-four constant
 * 32-bit words, K0, K1, ..., K63.  These words represent the first
 * thirty-two bits of the fractional parts of the cube roots of the
 * first sixty-four prime numbers.  In hex, these constant words are as
 * follows (from left to right):
 */
const SHA256K_HEX = [
	'428a2f98', '71374491', 'b5c0fbcf', 'e9b5dba5',
	'3956c25b', '59f111f1', '923f82a4', 'ab1c5ed5',
	'd807aa98', '12835b01', '243185be', '550c7dc3',
	'72be5d74', '80deb1fe', '9bdc06a7', 'c19bf174',
	'e49b69c1', 'efbe4786', '0fc19dc6', '240ca1cc',
	'2de92c6f', '4a7484aa', '5cb0a9dc', '76f988da',
	'983e5152', 'a831c66d', 'b00327c8', 'bf597fc7',
	'c6e00bf3', 'd5a79147', '06ca6351', '14292967',
	'27b70a85', '2e1b2138', '4d2c6dfc', '53380d13',
	'650a7354', '766a0abb', '81c2c92e', '92722c85',
	'a2bfe8a1', 'a81a664b', 'c24b8b70', 'c76c51a3',
	'd192e819', 'd6990624', 'f40e3585', '106aa070',
	'19a4c116', '1e376c08', '2748774c', '34b0bcb5',
	'391c0cb3', '4ed8aa4a', '5b9cca4f', '682e6ff3',
	'748f82ee', '78a5636f', '84c87814', '8cc70208',
	'90befffa', 'a4506ceb', 'bef9a3f7', 'c67178f2'
]

const SHA256K = SHA256K_HEX.map(i => HexToBinary(i, DWORD))

/**
 * SHA-256 use six logical functions, where each function
 * operates on 32-bit words, which are represented as x, y, and z.
 * The result of each function is a new 32-bit word.
 * 
 *  - CH( x, y, z) = (x AND y) XOR ( (NOT x) AND z)
 *  - MAJ( x, y, z) = (x AND y) XOR (x AND z) XOR (y AND z)
 *  - BSIG0(x) = ROTR^2(x) XOR ROTR^13(x) XOR ROTR^22(x)
 *  - BSIG1(x) = ROTR^6(x) XOR ROTR^11(x) XOR ROTR^25(x)
 *  - SSIG0(x) = ROTR^7(x) XOR ROTR^18(x) XOR SHR^3(x)
 *  - SSIG1(x) = ROTR^17(x) XOR ROTR^19(x) XOR SHR^10(x)
 */

/**
 * SHA256 logical function: CH
 * 
 * CH( x, y, z) = (x AND y) XOR ( (NOT x) AND z)
 * 
 * @param {number[]} X DWORD
 * @param {number[]} Y DWORD
 * @param {number[]} Z DWORD
 * 
 * @returns {number[]} DWORD
 */
function SHA256CH(X, Y, Z) {
	return XOR(AND(X, Y), AND(NOT(X), Z))
}

/**
 * SHA256 logical function: MAJ
 * 
 * MAJ( x, y, z) = (x AND y) XOR (x AND z) XOR (y AND z)
 * 
 * @param {number[]} X DWORD
 * @param {number[]} Y DWORD
 * @param {number[]} Z DWORD
 * 
 * @returns {number[]} DWORD
 */
function SHA256MAJ(X, Y, Z) {
	return XOR(XOR(AND(X, Y), AND(X, Z)), AND(Y, Z))
}

/**
 * SHA256 logical function: BSIG0
 * 
 * BSIG0(x) = ROTR^2(x) XOR ROTR^13(x) XOR ROTR^22(x)
 * 
 * @param {number[]} X DWORD
 * 
 * @returns {number[]} DWORD
 */
function SHA256BSIG0(X) {
	return XOR(XOR(RotateBits(-2, X), RotateBits(-13, X)), RotateBits(-22, X))
}

/**
 * SHA256 logical function: BSIG1
 * 
 * BSIG1(x) = ROTR^6(x) XOR ROTR^11(x) XOR ROTR^25(x)
 * 
 * @param {number[]} X DWORD
 * 
 * @returns {number[]} DWORD
 */
function SHA256BSIG1(X) {
	return XOR(XOR(RotateBits(-6, X), RotateBits(-11, X)), RotateBits(-25, X))
}

/**
 * SHA256 logical function: SSIG0
 * 
 * SSIG0(x) = ROTR^7(x) XOR ROTR^18(x) XOR SHR^3(x)
 * 
 * @param {number[]} X DWORD
 * 
 * @returns {number[]} DWORD
 */
function SHA256SSIG0(X) {
	return XOR(XOR(RotateBits(-7, X), RotateBits(-18, X)), ShiftBits(-3, X))
}

/**
 * SHA256 logical function: SSIG1
 * 
 * SSIG1(x) = ROTR^17(x) XOR ROTR^19(x) XOR SHR^10(x)
 * 
 * @param {number[]} X DWORD
 * 
 * @returns {number[]} DWORD
 */
function SHA256SSIG1(X) {
	return XOR(XOR(RotateBits(-17, X), RotateBits(-19, X)), ShiftBits(-10, X))
}

/**
 * RFC4634 [6.1] SHA-256 Initialization
 * 
 * @returns {number[][]} Returns SHA-256 H0 buffer
 */
function sha256createH0Buffer() {
	return [
		HexToBinary('6a09e667', DWORD),
		HexToBinary('bb67ae85', DWORD),
		HexToBinary('3c6ef372', DWORD),
		HexToBinary('a54ff53a', DWORD),
		HexToBinary('510e527f', DWORD),
		HexToBinary('9b05688c', DWORD),
		HexToBinary('1f83d9ab', DWORD),
		HexToBinary('5be0cd19', DWORD)
	]
}

/**
 * SHA256 split message into BLOCKS, variable N
 * 
 * @param {number[]} message must be of modulus 512 BLOCK
 * 
 * @returns {number[][]} Array of BLOCKS.
 */
function sha256splitBlocks(message) {
	if (message.length % BLOCK !== 0)
		throw new Error(`Message length must be of modulus BLOCK: ${BLOCK}, but got leftover: ${message.length % BLOCK}`)

	const count = message.length / BLOCK
	const result = []

	for (let i = 0; i < count; i++)
		result.push(message.slice(BLOCK * i, BLOCK * (i + 1)))

	return result
}

/**
 * SHA256 split block into DWORDS, variable M
 * 
 * @param {number[]} block must be of length 512 BLOCK
 * 
 * @returns {number[][]} Array of BLOCKS.
 */
function sha256splitDWords(block) {
	if (block.length !== BLOCK)
		throw new Error(`Block length must be: ${BLOCK}, but got: ${block.length}`)

	const count = block.length / DWORD
	const result = []

	for (let i = 0; i < count; i++)
		result.push(block.slice(DWORD * i, DWORD * (i + 1)))

	return result
}

/**
 * @param {number[]} data 
 * @returns {number[]} Array with length of multiple BLOCK 512 bits
 */
function sha256pad(data) {
	// How much bits are missing to complement 512 modulo,
	// 64 bits is for data length
	let offset = BLOCK - data.length % BLOCK - 64

	if (offset < 1)
		offset = BLOCK + offset

	// 1 followed by 0's to fit the length of offset
	const pad = [1, ...Array(offset - 1).fill(0)]

	// 64bit integer of data's length
	const length = IntToBinary(data.length, QWORD, false)
	
	// Put pieces together
	const result = Array().concat(data, pad, length)

	// Done
	return result
}

/**
 * @param {number[]} data
 */
function sha256(data) {
	data = sha256pad(data)
	const N = sha256splitBlocks(data) // data split into BLOCKs of 512 bits
	const Hash = sha256createH0Buffer() //8 DWORDS for intermediate hashes

	for (let i = 0; i < N.length; i++) {
		const M = sha256splitDWords(N[i]) // 16 DWORDS, split current BLOCK into DWORDs
		const W = Array(64) // 64 DWORDs, working message schedule

		let T1 = [] // DWORD temporary variable
		let T2 = [] // DWORD temporary variable

		// 1. Prepare the message schedule W:
		//    For t = 0 to 15
		//       Wt = M(i)t
		//    For t = 16 to 63
		//       Wt = SSIG1(W(t-2)) + W(t-7) + SSIG0(t-15) + W(t-16)
		for (let t = 0; t < 16; t++)
			W[t] = M[t]
		
		for (let t = 16; t < 64; t++)
			W[t] = BinarySum(SHA256SSIG1(W[t - 2]), W[t - 7], SHA256SSIG0(W[t - 15]), W[t - 16])

		// 2. Initialize the working variables:
		let A = Hash[0]
		let B = Hash[1]
		let C = Hash[2]
		let D = Hash[3]
		let E = Hash[4]
		let F = Hash[5]
		let G = Hash[6]
		let H = Hash[7]

		// 3. Perform the main hash computation:
		for(let t = 0; t < 64; t++) {
			T1 = BinarySum(H, SHA256BSIG1(E), SHA256CH(E, F, G), SHA256K[t], W[t])
			T2 = BinarySum(SHA256BSIG0(A), SHA256MAJ(A, B, C))

			H = G
			G = F
			F = E
			E = BinarySum(D, T1)
			D = C
			C = B
			B = A
			A = BinarySum(T1, T2)
		}

		// 4. Compute the intermediate hash value H(i):
		Hash[0] = BinarySum(A, Hash[0])
        Hash[1] = BinarySum(B, Hash[1])
        Hash[2] = BinarySum(C, Hash[2])
        Hash[3] = BinarySum(D, Hash[3])
        Hash[4] = BinarySum(E, Hash[4])
        Hash[5] = BinarySum(F, Hash[5])
        Hash[6] = BinarySum(G, Hash[6])
        Hash[7] = BinarySum(H, Hash[7])
	}

	// The result is last intermediate hash
	return Hash
}

/**
 * @param {number[]} data Input data, array of bits
 * @returns {string} HEX representation of sha256 result
 */
function sha256hex(data) {
	return BinaryToHex(sha256(data).flat())
}

/**************************************************
 *                 MAIN FUNCTION                  *
 **************************************************/

function main() {
	try {
		console.log('Running general tests.')
		tests()

		console.log('Running benchmark.')
		benchmark()
	} catch (e) {
		console.error('Error ocurred, cannot procceed')
		console.error(e)
		throw e // Rethrow for debugger
	}
}

main()

/**************************************************
 *                   BENCHMARK                    *
 **************************************************/

function writeOnSameLine(text) {
	// Support console redirected output
	if (typeof process.stdout.clearLine !== 'function') {
		console.log(text)
		return
	}

	process.stdout.clearLine();  // clear current text
	process.stdout.cursorTo(0);  // move cursor to beginning of line
	process.stdout.write(text);  // write text

}

/**
 * Here we measure various time intervals of hashing algorithms
 * 
 * For benchmarking hashing time, we first start with string:
 *  - 'Hello Crypto'
 * For every next cycle we add number 0, then 1, and up to 9, then repeat
 *  - Example on cycle 15: 'Hello Crypto012345678901234'
 * On cycle 500 subject string will be 512 chars which is:
 *    8 (+ 1 for pad) BLOCKs to process on each cycle
 */
function benchmark() {
	// Configurable variables
	const cycles = 500
	const salt_cycles = 100
	const StartingSubject = 'Hello Crypto'

	const log = (...args) => console.log('[Benchmark]', ...args) // logging proxy
	const salt_log = (...args) => console.log('[Benchmark-Salt]', ...args) // logging proxy

	log('Starting')
	log()
	log('Configuration:')
	log('\tCycles:', cycles)
	log('\tSalt_cycles:', salt_cycles)
	log('\tStartingSubject:', StartingSubject)
	log()
	benchmark_hash(log, md5, cycles, StartingSubject)
	log()
	benchmark_hash(log, sha1, cycles, StartingSubject)
	log()
	benchmark_hash(log, sha256, cycles, StartingSubject)
	log()
	benchmark_salt(salt_log, md5, salt_cycles, StartingSubject)
	salt_log()
	benchmark_salt(salt_log, sha1, salt_cycles, StartingSubject)
	salt_log()
	benchmark_salt(salt_log, sha256, salt_cycles, StartingSubject)
	log()
	log('Finished')
}

function benchmark_hash(log, func, cycles, subject) {
	const TimeStamp = process.hrtime() // This variable will be used to compare passed time

	log('Testing time for:', func.name)

	for (let i = 0; i < cycles; i++) {
		// Compute hash
		func(StringToBinary(subject))

		// Increment subject for the next run
		subject += i % 10

		if (i % 100 === 0)
			writeOnSameLine(`Cycle ${i} done`)
	}
	writeOnSameLine('')
	log('Time took:', process.hrtime(TimeStamp)[0], 'seconds')
}

/**
 * Benchmark of salts.
 * Subject string is not increased over cycles.
 * 
 * @param {function} log Logging function
 * @param {function} func Hashing function
 * @param {number} cycles Number of cycles for computing avarage
 * @param {string} subject Subject string
 */
function benchmark_salt(log, func, cycles, subject) {
	const randomBit = () => Math.random() > 0.5 ? 1 : 0
	const generateBuffer = (size) => Array(size).fill(null).map(() => randomBit())
	const msPassed = (start) => Math.floor(Number(process.hrtime.bigint() - start) / cycles / 10000) / 100

	const smallSalt = generateBuffer(8 * 3) // size of 3 chars, for example: 'abc'
	const largeSalt = generateBuffer(BLOCK) // Salt of size of whole BLOCK, ie 512 bits
	const binary = StringToBinary(subject)

	log('Running salt benchmark on:', func.name)
	log('No salt at all')

	let TimeStamp = process.hrtime.bigint() // This variable will be used to compare passed time

	for (let i = 0; i < cycles; i++) {
		func(binary)
	}

	log('Time took on avarage:', msPassed(TimeStamp), 'milliseconds.')
	log()
	log('Small salt: ' + BinaryToHex(smallSalt))
	TimeStamp = process.hrtime.bigint()

	for (let i = 0; i < cycles; i++) {
		func(Array().concat(smallSalt, binary))
	}

	log('Time took on avarage:', msPassed(TimeStamp), 'milliseconds.')
	log()
	log('Large salt: ' + BinaryToHex(largeSalt))
	TimeStamp = process.hrtime.bigint()

	for (let i = 0; i < cycles; i++) {
		func(Array().concat(largeSalt, binary))
	}

	log('Time took on avarage:', msPassed(TimeStamp), 'milliseconds.')
}

/**************************************************
 *                     TESTS                      *
 **************************************************/


/**
 * Casual assert-ion function, use debugger to catch it
 * 
 * @template T
 * @param {T} given 
 * @param {T} expected 
 * 
 * @throws {Error} Throws an error if given !== expected
 * 
 * @returns {void}
 */
function assert(given, expected) {
	if (typeof given !== typeof expected)
		throw new TypeError(`Type mismatch: typeof given: ${typeof given} !== typeof expected: ${typeof expected}`)

	if (expected instanceof Array === true) {
		if (given instanceof Array !== true)
			throw new TypeError('Given is not an array whereas expected is an array.')

		if (expected.length !== given.length)
			throw new RangeError(`Length mismatch: given.length: ${given.length} !== expected.length: ${expected.length}`)

		for (let i = 0; i < expected.length; i++) {
			assert(given[i], expected[i])
		}

		return
	}

	if (given !== expected)
		throw new Error(`Assert error: given: ${given} !== expected: ${expected}`)
}

/**
 * Array of 0's for test purposes
 * 
 * @param {number} length Length of array
 * 
 * @returns {0[]}
 */
function EmptyBits(length) {
	return Array(length).fill(0)
}

/**
 * Theses are general purpose tests.
 * They ensure that functions behave as expected.
 */
function tests() {
	// utils - ShiftBits
	assert(ShiftBits( 1, [0, 0, 0]), [0, 0, 0])
	assert(ShiftBits( 0, [1, 1, 1]), [1, 1, 1])
	assert(ShiftBits( 1, [1, 1, 1]), [1, 1, 0])
	assert(ShiftBits(-1, [1, 1, 1]), [0, 1, 1])
	assert(ShiftBits( 2, [1, 1, 1]), [1, 0, 0])
	assert(ShiftBits(-2, [1, 1, 1]), [0, 0, 1])

	// utils - BinarySum - verify that inputs are not modified
	const randomBuffer = () => Array(DWORD).fill(0).map(() => Math.random() > 0.5 ? 1 : 0)
	const buffers = Array(10).fill(0).map(() => randomBuffer())
	const originalBuffersDump = JSON.stringify(buffers)

	for (let i = 0; i < 10; i++)
		BinarySum(...RotateBits(i, buffers))

	const outcome = JSON.stringify(buffers)

	assert(outcome, originalBuffersDump)

	// utils - BinarySum
	const randomDWORD = () => Math.floor(Math.random() * Math.pow(2, DWORD))

	for (let i = 0; i < 10; i++) {
		const a = randomDWORD()
		const b = randomDWORD()
		
		assert(BinarySum(IntToBinary(a, DWORD), IntToBinary(b, DWORD)), IntToBinary(a + b, DWORD))
	}
	
	// utils - RotateBits
	const testShiftBitsBuffer = [1, 1, 1, 0, 0, 0]

	assert(RotateBits(0, []), [])
	assert(RotateBits(0, testShiftBitsBuffer), testShiftBitsBuffer)
	assert(RotateBits(testShiftBitsBuffer.length, testShiftBitsBuffer), testShiftBitsBuffer)
	assert(RotateBits(1, testShiftBitsBuffer), [1, 1, 0, 0, 0, 1])
	assert(RotateBits(3, testShiftBitsBuffer), [0, 0, 0, 1, 1, 1])

	// md5pad - length tests
	assert(md5pad(EmptyBits(  0)).length, 448)
	assert(md5pad(EmptyBits(100)).length, 448)
	assert(md5pad(EmptyBits(447)).length, 448)
	assert(md5pad(EmptyBits(448)).length, 960)
	assert(md5pad(EmptyBits(449)).length, 960)

	// md5pad - Magic bit '1'
	assert(md5pad(EmptyBits(  0))[  0], 1)
	assert(md5pad(EmptyBits(100))[100], 1)
	assert(md5pad(EmptyBits(448))[448], 1)

	// md5functions F G H I
	// All possible input combinations
	const X = [0, 0, 0, 0, 1, 1, 1, 1]
	const Y = [0, 0, 1, 1, 0, 0, 1, 1]
	const Z = [0, 1, 0, 1, 0, 1, 0, 1]

	// Expected outputs
	const F = [0, 1, 0, 1, 0, 0, 1, 1]
	const G = [0, 0, 1, 0, 0, 1, 1, 1]
	const H = [0, 1, 1, 0, 1, 0, 0, 1]
	const I = [1, 0, 0, 1, 1, 1, 0, 0]

	{
		// Pad inputs so that they are WORD long
		const padding = Array(DWORD - 8).fill(0)
		const x = X.concat(...padding)
		const y = Y.concat(...padding)
		const z = Z.concat(...padding)

		const f = md5functionF(x, y, z)
		const g = md5functionG(x, y, z)
		const h = md5functionH(x, y, z)
		const i = md5functionI(x, y, z)

		// Compare results to expected output
		for (let index = 0; index < 8; index++) {
			assert(f[index], F[index])
			assert(g[index], G[index])
			assert(h[index], H[index])
			assert(i[index], I[index])
		}
	}

	{ // md5 - my test cases
		assert(md5hex(StringToBinary('')),
			'5c38b75d5c9faf8579cc724d7a1d5219')
		assert(md5hex(StringToBinary('a')),
			'b4b6ef78018718e1f58707c82d799880')
		assert(md5hex(StringToBinary('abc')),
			'a82c53e034cf7a8b888d991a1f15ce10')
		assert(md5hex(StringToBinary('message digest')),
			'554dde1196561700928c8a8c1ab9a11e')
		assert(md5hex(StringToBinary('abcdefghijklmnopqrstuvwxyz')),
			'37a4ecafe5d5e58e43fee07fc983b385')
		assert(md5hex(StringToBinary('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')),
			'43546e9670ddb6b61b3ecf6ad40ee6ec')
		assert(md5hex(StringToBinary('12345678901234567890123456789012345678901234567890123456789012345678901234567890')),
			'5c798b4492be0f929bb26bca9dcc6cea')
	}

	if (false) { // TO BE FIXED!!!
	
		// md5 - RFC1321 [A.5] Test suite
		assert(md5hex(StringToBinary('')),
			'd41d8cd98f00b204e9800998ecf8427e')
		assert(md5hex(StringToBinary('a')),
			'0cc175b9c0f1b6a831c399e269772661')
		assert(md5hex(StringToBinary('abc')),
			'900150983cd24fb0d6963f7d28e17f72')
		assert(md5hex(StringToBinary('message digest')),
			'f96b697d7cb7938d525a2f31aaf161d0')
		assert(md5hex(StringToBinary('abcdefghijklmnopqrstuvwxyz')),
			'c3fcd3d76192e4007dfb496cca67e13b')
		assert(md5hex(StringToBinary('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')),
			'd174ab98d277d9f5a5611c2c9f419d9f')
		assert(md5hex(StringToBinary('12345678901234567890123456789012345678901234567890123456789012345678901234567890')),
			'57edf4a22be3c955ac49da2e2107b67a')

	}

	// sha1 - pad
	assert(sha1pad('0110000101100010011000110110010001100101'.split('').map(i => parseInt(i, 2))),
				   '01100001011000100110001101100100011001011'.split('').map(i => parseInt(i, 2)) // Original message + 1
				   .concat(Array(407).fill(0)) // Pad of 0's
				   .concat('0000000000000000000000000000000000000000000000000000000000101000'.split('').map(i => parseInt(i, 2)))) // 64 bit length

	// sha1 - H buffer
	assert(sha1createHBuffer()[1].join(''), '11101111110011011010101110001001')

	// sha1
	{
		assert(sha1hex(StringToBinary('abc')),
			'a9993e364706816aba3e25717850c26c9cd0d89d')

		assert(sha1hex(StringToBinary('abcdbcdecdefdefgefghfghighijhi')),
			'f9537c23893d2014f365adf8ffe33b8eb0297ed1')

		assert(sha1hex(StringToBinary('jkijkljklmklmnlmnomnopnopq')),
			'346fb528a24b48f563cb061470bcfd23740427ad')
			
			
	}
 
	// sha256
	{
		assert(sha256hex(StringToBinary('')),
			'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855')
		assert(sha256hex(StringToBinary('abc')),
			'ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad')
			assert(sha256hex(StringToBinary('message digest')),
				'f7846f55cf23e14eebeab5b4e1550cad5b509e3348fbc4efa3a1413d393cb650')
		assert(sha256hex(StringToBinary('abcdbcdecdefdefgefghfghighijhi')),
			'd578bbee0ee183a94170d4ff398cb29d06079a65101400771231f3fbb117c999')
		assert(sha256hex(StringToBinary('jkijkljklmklmnlmnomnopnopq')),
			'fb29fa721adddc89b7b58e1c6a5577359f7e879c48672275617fe11ceb851d57')
		assert(sha256hex(StringToBinary('Hello world')),
			'64ec88ca00b268e5ba1a35678a1b5316d212f4f366b2477232534a8aeca37f3c')

		assert(sha256hex(StringToBinary(Array(1000).fill("abc").join(''))),
			'328de8f1895f8bb09f6e6b4c2012ef2b2a6f067cd002794b750aa040a6f6d8bd')
	}
}



// EOF