Running general tests.
Running benchmark.
[Benchmark] Starting
[Benchmark]
[Benchmark] Configuration:
[Benchmark] 	Cycles: 1000
[Benchmark] 	Salt_cycles: 100
[Benchmark] 	StartingSubject: Hello Crypto
[Benchmark]
[Benchmark] Testing time for: md5
Cycle 0 done
Cycle 100 done
Cycle 200 done
Cycle 300 done
Cycle 400 done
Cycle 500 done
Cycle 600 done
Cycle 700 done
Cycle 800 done
Cycle 900 done

[Benchmark] Time took: 43 seconds
[Benchmark]
[Benchmark] Testing time for: sha1
Cycle 0 done
Cycle 100 done
Cycle 200 done
Cycle 300 done
Cycle 400 done
Cycle 500 done
Cycle 600 done
Cycle 700 done
Cycle 800 done
Cycle 900 done

[Benchmark] Time took: 73 seconds
[Benchmark]
[Benchmark] Testing time for: sha256
Cycle 0 done
Cycle 100 done
Cycle 200 done
Cycle 300 done
Cycle 400 done
Cycle 500 done
Cycle 600 done
Cycle 700 done
Cycle 800 done
Cycle 900 done

[Benchmark] Time took: 151 seconds
[Benchmark]
[Benchmark-Salt] Running salt benchmark on: md5
[Benchmark-Salt] No salt at all
[Benchmark-Salt] Time took on avarage: 4.91 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Small salt: 4feb10
[Benchmark-Salt] Time took on avarage: 4.91 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Large salt: 4e6ad7de91670b0782583e7f8206819cc7ce46c09027acdcaebb491708aebff5f4729ef7813fbad942dae508cb893fc9d5e3d412e7c32a37682f3ba6cc8e4aba
[Benchmark-Salt] Time took on avarage: 9.81 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Running salt benchmark on: sha1
[Benchmark-Salt] No salt at all
[Benchmark-Salt] Time took on avarage: 8.5 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Small salt: 75a1b8
[Benchmark-Salt] Time took on avarage: 8.5 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Large salt: 448e685aeed84b457b6b669796cef8bc6acb8520b5949ccdaa3b5425eba56c953ed6dd13f3111c1c29b4c7583ad6d9d7dd2c59099661e3df04db4986533bc3a7
[Benchmark-Salt] Time took on avarage: 17.1 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Running salt benchmark on: sha256
[Benchmark-Salt] No salt at all
[Benchmark-Salt] Time took on avarage: 17.44 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Small salt: 66077c
[Benchmark-Salt] Time took on avarage: 17.45 milliseconds.
[Benchmark-Salt]
[Benchmark-Salt] Large salt: 900114f09ec4e1568f638530897e5664044a208ca07716fb84e5c5010bb7bfaeaf9f7896aef98fa27b08c2ce6e6cadaafa3662fa7283cd7ce508f8b391854e14
[Benchmark-Salt] Time took on avarage: 35.64 milliseconds.
[Benchmark]
[Benchmark] Finished
