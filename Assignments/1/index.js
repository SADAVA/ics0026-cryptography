
// S.
// Title: Encipher/decipher image
// Author: Aleksander Thatsenko
// University: TalTech
// Date: 9th of March, 2021

// Desciption:
// This script will grap a file (in this example an image)
// Then encipher it and write it in an HEX format to text file.
// And afterwards grab the enciphered text file.
// Decode it from hex and decipher.
// And to finish up saves the result to disk.

'use strict'

// Dependencies
const fs = require('fs')
const { Transform } = require('stream')
const path = require('path')
const crypto = require('crypto')

// Settings
const InputImagePath = './image.jpg'
const OutputCipherimagePath = './cipherimage.txt'
const OutputImagePath = './image_deciphered.jpg'

const Settings = {
	Algorithm: 'aes-256-cbc',
	CipherKeyLength: 32,
}

function main() {
	// Configuration
	const Password = 'HelloWorldThisIsDummyKey'
	const salt = crypto.randomBytes(32)
	const iv = crypto.randomBytes(16)
	const cipherKey = crypto.scryptSync(Password, salt, Settings.CipherKeyLength)

	encipher(InputImagePath, OutputCipherimagePath, cipherKey, iv, () => {
		decipher(OutputCipherimagePath, OutputImagePath, cipherKey, iv, () => {
			console.log('All done!')
		})
	})
}

function encipher(input_path, output_path, key, iv, cb) {
	// Set up streams
	const aes = crypto.createCipheriv(Settings.Algorithm, key, iv)
	const input = fs.createReadStream(path.join(__dirname, input_path))
	const output = fs.createWriteStream(path.join(__dirname, output_path))

	// Start enciphering
	input
		.pipe(aes)
		.pipe(new ToHex())
		.pipe(output)
		// Wait for it to finish
		.on('finish', function () {
			console.log('Enciphered!');

			if (cb !== undefined) cb()
		});
}


function decipher(input_path, output_path, key, iv, cb) {
	// Set up streams
	const aes = crypto.createDecipheriv(Settings.Algorithm, key, iv)
	const input = fs.createReadStream(path.join(__dirname, input_path))
	const output = fs.createWriteStream(path.join(__dirname, output_path))

	// Start enciphering
	input
		.pipe(new FromHEX())
		.pipe(aes) 
		.pipe(output)
		// Wait for it to finish
		.on('finish', function () {
			console.log('Deciphered!');

			if (cb !== undefined) cb()
		});
}

class ToHex extends Transform {
	_transform(chunk, enc, cb) {
		const hex = chunk.toString('hex')
		this.push(hex)
		cb()
	}
}

class FromHEX extends Transform {
	_transform(chunk, enc, cb) {
		const buf = Buffer.from(chunk.toString(), 'hex')
		this.push(buf)
		cb()
	}
}

main()


// EOF